# Translation of etherape messages
# Copyright (C) 2000 Frederic Peters
# Frederic Peters <fpeters@debian.org>, 2000
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: 0.8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-16 17:05+0200\n"
"PO-Revision-Date: 2001-08-10 12:57+02:00\n"
"Last-Translator: Frederic Peters <fpeters@debian.org>\n"
"Language-Team: None\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# src/support.c:98 src/support.c:136
#: src/appdata.c:83
#, fuzzy, c-format
msgid "Could not load interface file '%s'!: %s"
msgstr "Impossible de trouver le fichier pixmap: %s"

# src/capture.c:986
#: src/datastructs.c:385
#, fuzzy, c-format
msgid "%s protocol not supported"
msgstr "Type de liaison pas encore supporté"

#: src/diagram.c:254
#, c-format
msgid ""
"Nodes: %d (on canvas: %d, shown: %u), Links: %d, Conversations: %ld, names "
"%ld, protocols %ld. Total Packets seen: %lu (in memory: %ld, on list %ld). "
"IP cache entries %ld. Canvas objs: %ld. Refreshed: %u ms"
msgstr ""

#: src/diagram.c:618
msgid "(Capture statistics unavailable in offline mode.)"
msgstr ""

#: src/diagram.c:673
#, c-format
msgid "Bogus statspos_t (%d) pref.pcap_stats_pos"
msgstr ""

#: src/diagram.c:700
#, c-format
msgid "SIGUSR1 received: exporting to %s"
msgstr ""

#: src/diagram.c:976
msgid "Canvas node null"
msgstr ""

# src/capture.c:409
#: src/diagram.c:985
#, fuzzy, c-format
msgid "Creating canvas_node: %s. Number of nodes %d"
msgstr "Création du noeud: %s. Nombre de noeuds: %d"

#: src/diagram.c:1049
msgid "Unknown value or node_size_variable"
msgstr ""

#: src/diagram.c:1686
msgid "Unknown value for link_size_variable"
msgstr ""

#: src/diagram.c:1732
#, c-format
msgid "Link main protocol: %s"
msgstr ""

#: src/diagram.c:1734
msgid "Link main protocol: unknown"
msgstr ""

#: src/diagram.c:1797
msgid ""
"'recv': packets received; 'drop': packets dropped by OS buffering; 'ifdrop': "
"packets dropped by interface or driver."
msgstr ""

#: src/info_windows.c:110 src/info_windows.c:794 src/info_windows.c:800
#, c-format
msgid "We could not load the interface! (%s)"
msgstr ""

# src/support.c:114
#: src/info_windows.c:116
#, fuzzy, c-format
msgid "Cannot create widget %s from file %s!"
msgstr "Impossible de créer un pixmap à partir du fichier: %s"

#: src/info_windows.c:200
msgid "No prot_name in on_prot_info_delete_event"
msgstr ""

#: src/info_windows.c:206
msgid "No prot_info_window in on_prot_info_delete_event"
msgstr ""

# src/interface.c:278
#: src/info_windows.c:438 src/pref_dialog.c:486
#, fuzzy
msgid "Protocol"
msgstr "Protocoles"

#: src/info_windows.c:439
msgid "Port"
msgstr ""

#: src/info_windows.c:440 src/node_windows.c:224
msgid "Inst Traffic"
msgstr ""

#: src/info_windows.c:441 src/node_windows.c:225
msgid "Accum Traffic"
msgstr ""

#: src/info_windows.c:442 src/node_windows.c:226
msgid "Avg Size"
msgstr ""

#: src/info_windows.c:443 src/node_windows.c:227
msgid "Last Heard"
msgstr ""

#: src/info_windows.c:444 src/node_windows.c:228
msgid "Packets"
msgstr ""

# src/interface.c:648
#: src/info_windows.c:929 src/info_windows.c:1012 src/info_windows.c:1013
#: src/info_windows.c:1022 src/info_windows.c:1023
#, fuzzy
msgid "Node timed out"
msgstr "Timeout pour les noeuds (ms)"

#: src/info_windows.c:989
msgid "B->A"
msgstr ""

#: src/info_windows.c:990
msgid "A->B"
msgstr ""

# src/interface.c:648
#: src/info_windows.c:996
#, fuzzy
msgid "Link timed out"
msgstr "Timeout pour les noeuds (ms)"

# src/main.c:61
#: src/main.c:96
msgid "don't display any node text identification"
msgstr "ne pas afficher d'identification pour les noeuds"

#: src/main.c:98
msgid "replay packets from file"
msgstr ""

#: src/main.c:98
msgid "<file to replay>"
msgstr ""

# src/main.c:70
#: src/main.c:100
msgid "set capture filter"
msgstr "définir le filtre de capture"

# src/main.c:70
#: src/main.c:100
msgid "<capture filter>"
msgstr "<filtre de capture>"

# src/main.c:67
#: src/main.c:102
msgid "set interface to listen to"
msgstr "définir l'interface sur laquelle écouter"

# src/main.c:67
#: src/main.c:102
msgid "<interface name>"
msgstr "<nom de l'interface>"

#: src/main.c:104
msgid "export to named file at end of replay"
msgstr ""

#: src/main.c:104 src/main.c:106
msgid "<file to export to>"
msgstr ""

#: src/main.c:106
msgid "export to named file on receiving USR1"
msgstr ""

#: src/main.c:108
msgid "Manually position nodes based on File"
msgstr ""

#: src/main.c:108
msgid "<list of nodes and their columns>"
msgstr ""

#: src/main.c:110
msgid "don't move nodes around (deprecated)"
msgstr ""

#: src/main.c:112
msgid "limits nodes displayed"
msgstr ""

#: src/main.c:112
msgid "<number of nodes>"
msgstr ""

# src/main.c:64
#: src/main.c:114
msgid "mode of operation"
msgstr "mode d'opération"

#: src/main.c:114
msgid "<link|ip|tcp>"
msgstr ""

# src/main.c:58
#: src/main.c:116
msgid "don't convert addresses to names"
msgstr "ne pas convertir les adresses en noms"

#: src/main.c:118
msgid "Disable informational messages"
msgstr ""

#: src/main.c:120
msgid "minimum packet delay in ms for reading capture files [cli only]"
msgstr ""

#: src/main.c:121 src/main.c:124
msgid "<delay>"
msgstr ""

#: src/main.c:123
msgid "maximum packet delay in ms for reading capture files [cli only]"
msgstr ""

#: src/main.c:126
msgid "uses the named libglade file for widgets"
msgstr ""

# src/main.c:70
#: src/main.c:126
#, fuzzy
msgid "<glade file>"
msgstr "<filtre de capture>"

#: src/main.c:128
msgid ""
"calculate statistics, but don't display nodes. If replaying, exit at end "
"[cli only]"
msgstr ""

#: src/main.c:130
msgid "run as the given user"
msgstr ""

# src/main.c:67
#: src/main.c:130
#, fuzzy
msgid "<username>"
msgstr "<nom de l'interface>"

# src/main.c:118
#: src/main.c:215
msgid "Unrecognized mode. Do etherape --help for a list of modes"
msgstr "Mode inconnu. Faites etherape --help pour la liste des modes"

#: src/main.c:297
msgid "DNS resolver initialization failed"
msgstr ""

#: src/main.c:310
msgid ""
"Obsolete setting found.\n"
"Please review and save your preferences to upgrade"
msgstr ""

#: src/main.c:356
#, c-format
msgid "Invalid position-file line: %s"
msgstr ""

#: src/main.c:360
#, c-format
msgid "Column number %ld out of range"
msgstr ""

#: src/main.c:384
#, c-format
msgid "Failed to read position file %s: %s"
msgstr ""

#: src/menus.c:50
#, c-format
msgid "get_interface result: '%s'"
msgstr ""

#: src/menus.c:52
msgid "No suitables interfaces for capture have been found"
msgstr ""

#: src/menus.c:62
msgid "Available interfaces for capture:"
msgstr ""

#: src/menus.c:101
#, fuzzy
msgid "Open Capture File"
msgstr "Etherape: Préférences"

#: src/menus.c:104 src/menus.c:133
msgid "_Cancel"
msgstr ""

# src/interface.c:258
#: src/menus.c:105
#, fuzzy
msgid "_Open"
msgstr "Ouvrir"

#: src/menus.c:130
msgid "Export to XML File"
msgstr ""

# src/interface.c:270
#: src/menus.c:134
#, fuzzy
msgid "_Save"
msgstr "Enregistrer"

# src/main.c:67
#: src/menus.c:180
#, fuzzy, c-format
msgid "Capture interface set to %s in GUI"
msgstr "définir l'interface sur laquelle écouter"

# src/capture.c:749
#: src/menus.c:206
#, fuzzy
msgid "Unsupported mode in on_mode_radio_activate"
msgstr "Mode ape non supporté dans get_link_id"

#: src/menus.c:233
#, c-format
msgid "Mode set to %s in GUI"
msgstr ""

#: src/menus.c:349
msgid "-unknown-"
msgstr ""

#: src/menus.c:412
msgid "Status already PLAY at gui_start_capture"
msgstr ""

#: src/menus.c:444 src/menus.c:474
#, c-format
msgid "Invalid mode: %d"
msgstr ""

#  Sets the appbar
#: src/menus.c:453
msgid "Reading data from "
msgstr ""

#: src/menus.c:460 src/menus.c:574
msgid "default interface"
msgstr ""

# src/interface.c:709
#: src/menus.c:465
#, fuzzy
msgid " in Data Link mode"
msgstr "Mode de la taille"

# src/interface.c:709
#: src/menus.c:468
#, fuzzy
msgid " in IP mode"
msgstr "Mode de la taille"

# src/interface.c:709
#: src/menus.c:471
#, fuzzy
msgid " in TCP mode"
msgstr "Mode de la taille"

# src/interface.c:510
#: src/menus.c:483
#, fuzzy
msgid "Diagram started"
msgstr "Diagramme"

#  Sets the appbar
#: src/menus.c:505
msgid "Paused"
msgstr ""

# src/interface.c:510
#: src/menus.c:507
#, fuzzy
msgid "Diagram paused"
msgstr "Diagramme"

#: src/menus.c:523
#, c-format
msgid "Replay from file '%s' completed."
msgstr ""

#: src/menus.c:553
#, c-format
msgid "Failed to stop capture: %s"
msgstr ""

# src/main.c:70
#  Sets the appbar
#: src/menus.c:567
#, fuzzy
msgid "Ready to capture from "
msgstr "définir le filtre de capture"

# src/interface.c:510
#: src/menus.c:579
#, fuzzy
msgid "Diagram stopped"
msgstr "Diagramme"

#: src/node_windows.c:222
msgid "Name"
msgstr ""

#: src/node_windows.c:223
msgid "Address"
msgstr ""

# src/callbacks.c:195 src/callbacks.c:206
#: src/pref_dialog.c:256
#, c-format
msgid "Adjustment value: %g. Radius multiplier %g"
msgstr "Valeur d'ajustement: %g. Multiplicateur de rayon: %g."

# src/callbacks.c:195 src/callbacks.c:206
#: src/pref_dialog.c:265
#, fuzzy, c-format
msgid "Adjustment value: %g. Inner ring scale %g"
msgstr "Valeur d'ajustement: %g. Multiplicateur de rayon: %g."

# src/main.c:76 src/main.c:79 src/main.c:82
#: src/pref_dialog.c:482
#, fuzzy
msgid "Color"
msgstr "couleur"

#: src/preferences.c:373
#, fuzzy, c-format
msgid "Preferences saved to %s"
msgstr "Préférences enregistrées"

#: src/preferences.c:377
#, c-format
msgid "Error saving preferences to '%s': %s"
msgstr ""

#: src/capture/capctl.c:88 src/capture/capctl.c:121
msgid "Failed to receive message from packet-capture process"
msgstr ""

#: src/capture/capctl.c:97
msgid "Failed to send message to packet-capture process"
msgstr ""

#: src/capture/capctl.c:103
msgid "Failed to receive packet from packet-capture process"
msgstr ""

#: src/capture/capctl.c:130
#, c-format
msgid "Failed to set %s environment variable to '%s': %s"
msgstr ""

#: src/capture/capctl.c:142
#, c-format
msgid "Unknown user '%s'"
msgstr ""

#: src/capture/capctl.c:147
#, c-format
msgid "Failed to switch to user '%s' (uid=%lu, gid=%lu): %s"
msgstr ""

#: src/capture/capctl.c:460
#, c-format
msgid ""
"%s uses unsupported link type %d, cannot proceed.  Please choose another "
"source."
msgstr ""

#: src/capture/capctl.c:470
msgid ""
"This device does not support link-layer mode.  Please use IP or TCP modes."
msgstr ""

#: src/names/names.c:109
#, c-format
msgid ""
"not enough captured data, terminating protocol decode for '%s' (level %d)"
msgstr ""

#: src/names/names.c:113
#, c-format
msgid "not enough captured data, terminating protocol decode at level %d"
msgstr ""

# src/capture.c:749
#: src/names/names.c:221
#, fuzzy
msgid "Unsupported ape mode in fill_node_id"
msgstr "Mode ape non supporté dans get_link_id"

#: src/stats/decode_proto.c:192
#, c-format
msgid "Link type is %s"
msgstr ""

#: src/stats/decode_proto.c:257
msgid "Data link entry not initialized"
msgstr ""

#: src/stats/decode_proto.c:556
msgid "Radiotap:captured size too small, packet discarded"
msgstr ""

#: src/stats/decode_proto.c:584
msgid "PPI:captured size too small, packet discarded"
msgstr ""

#: src/stats/decode_proto.c:612
#, c-format
msgid "PPI:unsupported link type %u, packet discarded"
msgstr ""

#: src/stats/decode_proto.c:690
msgid "wlan:captured size too small (less than 10 bytes), packet discarded"
msgstr ""

#: src/stats/decode_proto.c:713
#, c-format
msgid "Invalid tofromds field in WLAN packet: 0x%x"
msgstr ""

#: src/stats/decode_proto.c:743 src/stats/decode_proto.c:772
#, c-format
msgid "wlan:captured size too small (read %u, needed %u), packet discarded"
msgstr ""

#: src/stats/decode_proto.c:756
#, c-format
msgid "wlan:captured size too small (read %u, needed %u), RTS packet discarded"
msgstr ""

#: src/stats/decode_proto.c:805
#, c-format
msgid "wlan:frame type 0x%x is reserved, decode aborted"
msgstr ""

#: src/stats/decode_proto.c:809
#, c-format
msgid "wlan:unknown frame type 0x%x, decode aborted"
msgstr ""

#: src/stats/links.c:228
msgid "Queuing link for remove"
msgstr ""

# src/capture.c:438 src/capture.c:443
#: src/stats/links.c:284
#, fuzzy, c-format
msgid "New link: %s. Number of links %d"
msgstr "Création de la liaison: %s-%s. Nombre de liaisons: %d"

#: src/stats/links.c:364
#, c-format
msgid "Updated links. Active links %d"
msgstr ""

# src/capture.c:409
#: src/stats/node.c:108
#, fuzzy, c-format
msgid "New node: %s. Number of nodes %d"
msgstr "Création du noeud: %s. Nombre de noeuds: %d"

# src/capture.c:409
#: src/stats/node.c:132
#, fuzzy, c-format
msgid "delete node: %s. Number of nodes %d"
msgstr "Création du noeud: %s. Nombre de noeuds: %d"

#: src/stats/node.c:239
#, c-format
msgid "Queuing node '%s' for remove"
msgstr ""

#: src/stats/node.c:543
#, c-format
msgid "Updated nodes. Active nodes %d"
msgstr ""

# src/capture.c:749
#: src/stats/node_id.c:69
#, fuzzy
msgid "Unsupported ape mode in node_id_compare"
msgstr "Mode ape non supporté dans get_link_id"

#: src/stats/util.c:313
#, c-format
msgid "%ld\" ago"
msgstr ""

#: src/stats/util.c:318
#, c-format
msgid "%ld'%ld\" ago"
msgstr ""

# src/interface.c:350
#: glade/etherape.ui:12
#, fuzzy
msgid "A Graphical Network Browser."
msgstr ""
"Un navigateur de réseau graphique.\n"
"Web: http://etherape.sourceforge.net"

#: glade/etherape.ui:40
msgid ""
"Vincent van Adrighem <vincent@dirck.mine.nu>\n"
"AlexL <alexl000@users.sourceforge.net>\n"
"Gôrkem Cetin <gorkem@gelecek.com.tr>\n"
"Javier Fernandez-Sanguino Peña\n"
"Chris Leick <c.leick@vollbio.de>\n"
"Frederic Peters <fpeters@debian.org>\n"
"Daniel Nylander\n"
msgstr ""

# src/interface.c:23
#: glade/etherape.ui:176
#, fuzzy
msgid "_File"
msgstr "Nouveau fichier"

# src/interface.c:278
#: glade/etherape.ui:195
#, fuzzy
msgid "_export"
msgstr "Protocoles"

#: glade/etherape.ui:234
#, fuzzy
msgid "_Capture"
msgstr "Capture"

# src/interface.c:709
#: glade/etherape.ui:243
#, fuzzy
msgid "_Mode"
msgstr "Mode de la taille"

#: glade/etherape.ui:255
msgid "_Link Layer"
msgstr ""

#: glade/etherape.ui:274
msgid "_IP"
msgstr ""

#: glade/etherape.ui:288
msgid "_TCP"
msgstr ""

#: glade/etherape.ui:304
msgid "_Interfaces"
msgstr ""

#: glade/etherape.ui:343
msgid "_Pause"
msgstr ""

#: glade/etherape.ui:356
msgid "St_op"
msgstr ""

# src/interface.c:23
#: glade/etherape.ui:375
#, fuzzy
msgid "_View"
msgstr "Nouveau fichier"

# src/interface.c:278
#: glade/etherape.ui:385
#, fuzzy
msgid "_Protocols"
msgstr "Protocoles"

# src/interface.c:709
#: glade/etherape.ui:394
#, fuzzy
msgid "_Nodes"
msgstr "Mode de la taille"

#: glade/etherape.ui:409
msgid "_Full Screen"
msgstr ""

# src/interface.c:46
#: glade/etherape.ui:420
msgid "_Toolbar"
msgstr "Barre d'_outils"

# src/interface.c:53
#: glade/etherape.ui:431
msgid "_Legend"
msgstr "_Légende"

# src/interface.c:60
#: glade/etherape.ui:442
msgid "_Status Bar"
msgstr "Barre d'_état"

#: glade/etherape.ui:456
msgid "Refresh"
msgstr ""

#: glade/etherape.ui:474
msgid "_Help"
msgstr ""

#: glade/etherape.ui:522
#, fuzzy
msgid "Start capture"
msgstr "Capture"

#: glade/etherape.ui:523
msgid "Start"
msgstr ""

#: glade/etherape.ui:537
#, fuzzy
msgid "Next"
msgstr "Pas de texte"

#: glade/etherape.ui:552
#, fuzzy
msgid "Pause capture"
msgstr "Capture"

#: glade/etherape.ui:553
msgid "Pause"
msgstr ""

#: glade/etherape.ui:568
#, fuzzy
msgid "Stop capture"
msgstr "Capture"

#: glade/etherape.ui:569
msgid "Stop"
msgstr ""

#: glade/etherape.ui:594
#, fuzzy
msgid "Preferences (Ctrl-P)"
msgstr "Préférences enregistrées"

# src/interface.c:278
#: glade/etherape.ui:595
#, fuzzy
msgid "Pref."
msgstr "Protocoles"

#: glade/etherape.ui:611
#, fuzzy
msgid "Display the protocols window"
msgstr "Affiche ou cache la barre d'outils"

# src/interface.c:278
#: glade/etherape.ui:612
#, fuzzy
msgid "Prot."
msgstr "Protocoles"

# src/interface.c:709
#: glade/etherape.ui:626 glade/etherape.ui:1865 glade/etherape.ui:1988
#: glade/etherape.ui:2781
#, fuzzy
msgid "Nodes"
msgstr "Mode de la taille"

# src/interface.c:278
#: glade/etherape.ui:690
msgid "Protocols"
msgstr "Protocoles"

#: glade/etherape.ui:741
#, fuzzy
msgid "Select color"
msgstr "Choisir la police"

#: glade/etherape.ui:826
msgid "Topmost recognized protocol"
msgstr ""

#: glade/etherape.ui:829
msgid "Level 2 (Eg: ETH_II)"
msgstr ""

#: glade/etherape.ui:832
msgid "Level 3 (Eg: IP)"
msgstr ""

#: glade/etherape.ui:835
msgid "Level 4 (Eg: TCP)"
msgstr ""

#: glade/etherape.ui:838
msgid "Level 5 (Eg: HTTP)"
msgstr ""

#: glade/etherape.ui:849
msgid "Instant. traffic (In+Out)"
msgstr ""

#: glade/etherape.ui:852
msgid "Instant. traffic (Inbound)"
msgstr ""

#: glade/etherape.ui:855
msgid "Instant. traffic (Outbound)"
msgstr ""

#: glade/etherape.ui:858
msgid "Instant. packets (In+Out)"
msgstr ""

#: glade/etherape.ui:861
msgid "Accum. traffic (In+Out)"
msgstr ""

#: glade/etherape.ui:864
msgid "Accum. traffic (Inbound)"
msgstr ""

#: glade/etherape.ui:867
msgid "Accum. traffic (Outbound)"
msgstr ""

#: glade/etherape.ui:870
msgid "Accum. packets (In+Out)"
msgstr ""

#: glade/etherape.ui:873
msgid "Average pkt size (In+Out)"
msgstr ""

#: glade/etherape.ui:884
msgid "(Off)"
msgstr ""

#: glade/etherape.ui:887
msgid "Upper left"
msgstr ""

#: glade/etherape.ui:890
msgid "Upper right"
msgstr ""

#: glade/etherape.ui:893
msgid "Lower left"
msgstr ""

#: glade/etherape.ui:896
msgid "Lower right"
msgstr ""

# src/interface.c:698
#: glade/etherape.ui:907
msgid "Linear"
msgstr "Linéaire"

# src/interface.c:701
#: glade/etherape.ui:910
msgid "Logarithmic"
msgstr "Lograrithmique"

# src/interface.c:704
#: glade/etherape.ui:913
msgid "Square Root"
msgstr "Racine carrée"

#: glade/etherape.ui:919
#, fuzzy
msgid "EtherApe: Preferences"
msgstr "Etherape: Préférences"

# src/interface.c:270
#: glade/etherape.ui:935
msgid "Save"
msgstr "Enregistrer"

#: glade/etherape.ui:940
msgid "Saves changes to preferences file"
msgstr ""

#: glade/etherape.ui:957
msgid "Confirm changes"
msgstr ""

#: glade/etherape.ui:974
msgid "Cancel changes"
msgstr ""

#: glade/etherape.ui:1017
#, fuzzy
msgid "Protocol Stack _Level"
msgstr "Niveau du protocole"

#: glade/etherape.ui:1031
msgid "Set what level of the protocol stack is displayed in the legend"
msgstr "Défini le niveau du protocole affiché dans la légende"

#: glade/etherape.ui:1061
msgid "The statistic used to compute node and link sizes"
msgstr ""

#: glade/etherape.ui:1063
msgid "Size _Variable"
msgstr ""

#: glade/etherape.ui:1077
msgid ""
"Set the kind of instantaneous or accumulated traffic that the node radius "
"indicates"
msgstr ""

#: glade/etherape.ui:1108
msgid "_Inner Ring Scale"
msgstr ""

#: glade/etherape.ui:1122
msgid "Inner ring radius as a fraction of outer ring size"
msgstr ""

#: glade/etherape.ui:1149
msgid "Display pcap _stats:"
msgstr ""

#: glade/etherape.ui:1162
msgid ""
"Set where to display pcap stats: packets received, dropped by OS buffering, "
"and dropped by the interface or driver"
msgstr ""

#: glade/etherape.ui:1194
msgid "Node _Radius Multiplier"
msgstr ""

# src/interface.c:709
#: glade/etherape.ui:1235
#, fuzzy
msgid "Size _Mode"
msgstr "Mode de la taille"

#: glade/etherape.ui:1248
msgid ""
"Choose how node radius and link width are calculated as a function of "
"average traffic"
msgstr ""
"Défini comment le rayon des noeuds et la largeur des liaisons sont "
"calculésen fonction du traffic moyen"

#: glade/etherape.ui:1276
msgid "_Hide node names"
msgstr ""

#: glade/etherape.ui:1280
msgid "Toggle whether the node names are displayed on top of the nodes"
msgstr ""

#: glade/etherape.ui:1293
msgid "_Group unknown ports"
msgstr ""

#: glade/etherape.ui:1320
msgid "_Name Resolution"
msgstr ""

#: glade/etherape.ui:1324
msgid ""
"Enable name resolution. If unchecked, all addresses are in numeric form."
msgstr ""

# src/main.c:70
#: glade/etherape.ui:1354
#, fuzzy
msgid "_Capture filter"
msgstr "Filtre de capture"

# src/main.c:70
#: glade/etherape.ui:1367
#, fuzzy
msgid "Sets the capture filter"
msgstr "définir le filtre de capture"

# src/interface.c:648
#: glade/etherape.ui:1398
#, fuzzy
msgid "Node Label _Font"
msgstr "Timeout pour les noeuds (ms)"

# src/interface.c:648
#: glade/etherape.ui:1413
#, fuzzy
msgid "Node Label Font"
msgstr "Timeout pour les noeuds (ms)"

#: glade/etherape.ui:1439
msgid "Node Label C_olor"
msgstr ""

#: glade/etherape.ui:1454
#, fuzzy
msgid "Choose the color used to display node labels"
msgstr "Défini la police utilisée pour afficher le texte dans le diagramme"

#: glade/etherape.ui:1455
msgid "Pick a Color "
msgstr ""

#: glade/etherape.ui:1480
msgid "C_entral Node"
msgstr ""

#: glade/etherape.ui:1493
msgid "Optional central node"
msgstr ""

#: glade/etherape.ui:1523
msgid "Background Image"
msgstr ""

#: glade/etherape.ui:1534
msgid "Use _background image"
msgstr ""

#: glade/etherape.ui:1538
msgid "Enable selection of a background image for the main display"
msgstr ""

# src/interface.c:510
#: glade/etherape.ui:1597
msgid "Diagram"
msgstr "Diagramme"

#: glade/etherape.ui:1646
msgid "A_dd row"
msgstr ""

#: glade/etherape.ui:1650
msgid ""
"Add a new row to the list of colors that can be used to represent protocols"
msgstr ""

# src/main.c:76
#: glade/etherape.ui:1662
#, fuzzy
msgid "R_emove row"
msgstr "définir la couleur des noeuds"

#: glade/etherape.ui:1666
msgid "remove the selected row"
msgstr ""

# src/interface.c:278
#: glade/etherape.ui:1681
#, fuzzy
msgid "_Edit row"
msgstr "Protocoles"

#: glade/etherape.ui:1685
msgid "Edite the current row"
msgstr ""

# src/main.c:76 src/main.c:79 src/main.c:82
#: glade/etherape.ui:1720
#, fuzzy
msgid "Colors"
msgstr "couleur"

# src/interface.c:570
#: glade/etherape.ui:1755
#, fuzzy
msgid "Diagram Refresh _Period (ms)"
msgstr "Période de rafraichissement (ms)"

#: glade/etherape.ui:1768
msgid "Refresh diagram every this many miliseconds"
msgstr "Nombre de millisecondes entre les rafraîchissement du diagramme"

# src/interface.c:545
#: glade/etherape.ui:1798
msgid "Averaging Time (ms)"
msgstr "Temps moyen (ms)"

#: glade/etherape.ui:1810
msgid "Packet information is averaged for this amount of time"
msgstr ""
"La moyenne sur les informations des paquets est effectuée pour cette "
"quantité de temps"

#: glade/etherape.ui:1878
#, fuzzy
msgid ""
"Remove this node from the diagram after this much time. 0 means never "
"timeout."
msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

#: glade/etherape.ui:1908 glade/etherape.ui:2030
msgid "Links"
msgstr ""

#: glade/etherape.ui:1921
#, fuzzy
msgid ""
"Remove this link from the diagram after this much time. 0 means never "
"timeout."
msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

# src/interface.c:674
#: glade/etherape.ui:1949
#, fuzzy
msgid "<i>Diagram Timeouts (s)</i>"
msgstr "Timeout pour les liaisons (ms)"

#: glade/etherape.ui:2000
#, fuzzy
msgid ""
"Expire protocol statistics for a node after this much time. 0 means never "
"timeout."
msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

#: glade/etherape.ui:2043
#, fuzzy
msgid ""
"Remove this link from from statistics after this much time without traffic. "
"0 means never timeout."
msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

#: glade/etherape.ui:2044
msgid "10"
msgstr ""

# src/interface.c:674
#: glade/etherape.ui:2073
#, fuzzy
msgid "<i>Statistics Timeouts (s)</i>"
msgstr "Timeout pour les liaisons (ms)"

#: glade/etherape.ui:2112
#, fuzzy
msgid "Statistics"
msgstr "Niveau du protocole"

#: glade/etherape.ui:2125
#, fuzzy
msgid "Remove this protocol from memory after this much time. "
msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

# src/interface.c:648
#: glade/etherape.ui:2156
#, fuzzy
msgid "<i>Global Protocol Timeouts (s)</i>"
msgstr "Timeout pour les noeuds (ms)"

#: glade/etherape.ui:2199
msgid "Timings"
msgstr ""

#: glade/etherape.ui:2229
msgid "WWW"
msgstr ""

#: glade/etherape.ui:2235
#, fuzzy
msgid "EtherApe: assign protocol"
msgstr "Etherape: Préférences"

# src/interface.c:278
#: glade/etherape.ui:2311
#, fuzzy
msgid "Row _protocol name(s) ..."
msgstr "Protocoles"

#: glade/etherape.ui:2327
#, fuzzy
msgid "Type the protocol name"
msgstr "Affiche ou cache la barre d'outils"

# src/main.c:76 src/main.c:79 src/main.c:82
#: glade/etherape.ui:2365
#, fuzzy
msgid "Row _Color"
msgstr "couleur"

#: glade/etherape.ui:2494
msgid "Numeric Name:"
msgstr ""

#: glade/etherape.ui:2506
msgid "Resolved Name:"
msgstr ""

#: glade/etherape.ui:2517
msgid "node A"
msgstr ""

#: glade/etherape.ui:2528
msgid "node B"
msgstr ""

#: glade/etherape.ui:2587
msgid "Total"
msgstr ""

#: glade/etherape.ui:2599
msgid "Inbound"
msgstr ""

#: glade/etherape.ui:2612
msgid "Outbound"
msgstr ""

#: glade/etherape.ui:2625 glade/etherape.ui:2938
msgid "Instantaneous"
msgstr ""

#: glade/etherape.ui:2637 glade/etherape.ui:2979
msgid "Accumulated"
msgstr ""

#: glade/etherape.ui:2693
msgid "Average size"
msgstr ""

#: glade/etherape.ui:2796
msgid "_Show all nodes"
msgstr ""

#: glade/etherape.ui:2801
msgid "Controls display of nodes timed out from diagram, but still in memory"
msgstr ""

#: glade/etherape.ui:2861
msgid "name"
msgstr ""

#: glade/etherape.ui:2886
msgid "Last Heard: "
msgstr ""

#: glade/etherape.ui:2899
msgid "label73"
msgstr ""

#: glade/etherape.ui:2952 glade/etherape.ui:2993
msgid "test label"
msgstr ""

#: glade/etherape.ui:3023
#, fuzzy
msgid "EtherApe: Protocols"
msgstr "Etherape: Préférences"

# src/main.c:76 src/main.c:79 src/main.c:82
#, fuzzy
#~ msgid "Add color"
#~ msgstr "couleur"

# src/main.c:76 src/main.c:79 src/main.c:82
#, fuzzy
#~ msgid "Change color"
#~ msgstr "couleur"

# src/callbacks.c:195 src/callbacks.c:206
#, fuzzy, c-format
#~ msgid "Adjustment value: %g. Link-node ratio %g"
#~ msgstr "Valeur d'ajustement: %g. Multiplicateur de rayon: %g."

#, fuzzy
#~ msgid ""
#~ "Delete this node from memory after this much time. 0 means never timeout."
#~ msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

# src/interface.c:648
#, fuzzy
#~ msgid "<i>Node Timeouts (s)</i>"
#~ msgstr "Timeout pour les noeuds (ms)"

#, fuzzy
#~ msgid ""
#~ "Delete this link from memory after this much time. 0 means never timeout."
#~ msgstr "Supprime ce noeud après cette quantité de temps. 0 signifie jamais."

# src/interface.c:107
#  append_etype_prot
# 
#  * Archivo de cadenas traducibles generado por Glade.
#  * Añada este archivo a su POTFILES.in de su proyecto.
#  * NO lo compile como parte de su aplicación.
# 
#, fuzzy
#~ msgid "EtherApe"
#~ msgstr "Etherape"

# src/interface.c:348
#, fuzzy
#~ msgid "Copyright 2001-2018 Juan Toledo, Riccardo Ghetta"
#~ msgstr "Copyright 2000 Juan Toledo"

# src/interface.c:709
#, fuzzy
#~ msgid "Set IP mode"
#~ msgstr "Mode de la taille"

# src/interface.c:709
#, fuzzy
#~ msgid "Set TCP mode"
#~ msgstr "Mode de la taille"

#, fuzzy
#~ msgid "Show or hide the protocols window"
#~ msgstr "Affiche ou cache la barre d'outils"

#, fuzzy
#~ msgid "Show or hide the nodes window"
#~ msgstr "Affiche ou cache la barre d'outils"

#~ msgid "Show or hide the toolbar"
#~ msgstr "Affiche ou cache la barre d'outils"

#~ msgid "Show or hide the legend"
#~ msgstr "Affiche ou cache la légende"

#~ msgid "Show or hide the status bar"
#~ msgstr "Affiche ou cache la barre d'état"

# src/capture.c:931
#, fuzzy
#~ msgid ""
#~ "Error opening %s : %s\n"
#~ "- perhaps you need to be root?"
#~ msgstr "Erreur à l'ouverture de %s : %s (vous devriez peut-être être root?)"

# src/capture.c:922
#, fuzzy
#~ msgid "Error opening %s : %s"
#~ msgstr "Erreur au périphérique: %s"

#, fuzzy
#~ msgid "%s opened for offline capture"
#~ msgstr "Capture"

# src/capture.c:948
#, fuzzy
#~ msgid "Unable to parse filter string (%s). Filter ignored."
#~ msgstr "Impossible d'analyser le filtre (%s)."

# src/capture.c:954
#, fuzzy
#~ msgid "Can't install filter (%s). Filter ignored."
#~ msgstr "Impossible d'installer le filtre (%s)."

#, fuzzy
#~ msgid "Starting live capture"
#~ msgstr "Capture"

#, fuzzy
#~ msgid "Using timers for live capture"
#~ msgstr "Capture"

#, fuzzy
#~ msgid "Starting offline capture"
#~ msgstr "Capture"

#, fuzzy
#~ msgid "Pausing offline capture"
#~ msgstr "Capture"

#, fuzzy
#~ msgid "Stopping live capture"
#~ msgstr "Capture"

#, fuzzy
#~ msgid "Stopping offline capture"
#~ msgstr "Capture"

# src/capture.c:948
#, fuzzy
#~ msgid "Unable to  parse line %s"
#~ msgstr "Impossible d'analyser le filtre (%s)."

# src/capture.c:979
#~ msgid "Mode not available in this device"
#~ msgstr "Mode non disponible pour ce périphérique."

# src/main.c:76 src/main.c:79 src/main.c:82
#, fuzzy
#~ msgid "Columns"
#~ msgstr "couleur"

# src/interface.c:278
#, fuzzy
#~ msgid "_Protocol"
#~ msgstr "Protocoles"

# src/main.c:64
#, fuzzy
#~ msgid "<ethernet|fddi|ip|tcp>"
#~ msgstr "<ethernet|ip|tcp|udp>"

# src/capture.c:922
#~ msgid "Error getting device: %s"
#~ msgstr "Erreur au périphérique: %s"

# src/capture.c:713
#~ msgid "Reached default in get_node_id"
#~ msgstr "Comportement par défaut atteint dans get_node_id"

# src/capture.c:1005
#~ msgid "Ape mode not yet supported"
#~ msgstr "Mode ape pas encore supporté"

# src/interface.c:709
#, fuzzy
#~ msgid " in Token Ring mode"
#~ msgstr "Mode de la taille"

# src/interface.c:107
#, fuzzy
#~ msgid " in Ethernet mode"
#~ msgstr "Etherape"

# src/interface.c:709
#, fuzzy
#~ msgid "Set Token Ring mode"
#~ msgstr "Mode de la taille"

# src/interface.c:709
#, fuzzy
#~ msgid "Token _Ring"
#~ msgstr "Mode de la taille"

# src/interface.c:709
#, fuzzy
#~ msgid "Set FDDI mode"
#~ msgstr "Mode de la taille"

# src/interface.c:107
#, fuzzy
#~ msgid "Set Ethernet mode"
#~ msgstr "Etherape"

# src/interface.c:107
#, fuzzy
#~ msgid "_Ethernet"
#~ msgstr "Etherape"

# src/main.c:70
#, fuzzy
#~ msgid "set input file"
#~ msgstr "définir le filtre de capture"

# src/main.c:73
#~ msgid "do not fade old links"
#~ msgstr "ne pas assombrir les anciennes liaisons"

# src/main.c:76
#~ msgid "set the node color"
#~ msgstr "définir la couleur des noeuds"

# src/main.c:76 src/main.c:79 src/main.c:82
#, fuzzy
#~ msgid "<color>"
#~ msgstr "couleur"

# src/main.c:82
#~ msgid "set the text color"
#~ msgstr "définir la couleur du texte"

# src/capture.c:749
#, fuzzy
#~ msgid "Unsupported ape mode in print_mem"
#~ msgstr "Mode ape non supporté dans get_link_id"

# src/capture.c:438 src/capture.c:443
#, fuzzy
#~ msgid "Creating canvas_link: %s. Number of links %d"
#~ msgstr "Création de la liaison: %s-%s. Nombre de liaisons: %d"

# src/interface.c:709
#, fuzzy
#~ msgid "Set UDP mode"
#~ msgstr "Mode de la taille"

#, fuzzy
#~ msgid "_Font"
#~ msgstr "Police"

# src/interface.c:278
#, fuzzy
#~ msgid "Protocol Info"
#~ msgstr "Protocoles"

#~ msgid "Select Font"
#~ msgstr "Choisir la police"

# src/interface.c:436
#, fuzzy
#~ msgid "This message is not here yet. (Don't tell anybody you saw it ;-) )"
#~ msgstr ""
#~ "Cette fonction n'est pas encore présente. (Ne le dites à personne ;-) )"

# src/main.c:70
#, fuzzy
#~ msgid "EtherApe: Select capture file"
#~ msgstr "définir le filtre de capture"

#, fuzzy
#~ msgid "EtherApe: Select Capture File"
#~ msgstr "Etherape: Préférences"

# src/main.c:70
#, fuzzy
#~ msgid "Select capture file"
#~ msgstr "définir le filtre de capture"

# src/main.c:70
#, fuzzy
#~ msgid "Optionally, set a capture filter"
#~ msgstr "définir le filtre de capture"

# src/interface.c:23
#, fuzzy
#~ msgid "File:"
#~ msgstr "Nouveau fichier"

# src/interface.c:23
#, fuzzy
#~ msgid "Filter:"
#~ msgstr "Nouveau fichier"

# src/main.c:67
#, fuzzy
#~ msgid "skipping interface %s: is down"
#~ msgstr "définir l'interface sur laquelle écouter"

# src/capture.c:942
#~ msgid "Can't use filter:  Couldn't obtain netmask info (%s)."
#~ msgstr ""
#~ "Impossible d'utiliser le filtre: impossible d'obtenir l'info de netmask "
#~ "(%s)."

# src/capture.c:409
#, fuzzy
#~ msgid "Removing node: %s. Number of nodes %d"
#~ msgstr "Suppression du noeud: %s. Nombre de noeuds: %d"

# src/capture.c:438 src/capture.c:443
#~ msgid "Removing link. Number of links %d"
#~ msgstr "Suppression de la liaison. Nombre de liaisons: %d"

# src/capture.c:499
#~ msgid "Null packet in check_packet"
#~ msgstr "Paquet NULL dans check_packet"

# src/capture.c:948
#, fuzzy
#~ msgid "Unable to parse color string %s for new protocol %s"
#~ msgstr "Impossible d'analyser le filtre (%s)."

# src/interface.c:648
#, fuzzy
#~ msgid "Diagram Node Timeout (ms)"
#~ msgstr "Timeout pour les noeuds (ms)"

# src/interface.c:622
#~ msgid "Max. Link Width"
#~ msgstr "Largeur de liaison maximale"

# src/interface.c:596
#~ msgid "Max. Node Radius"
#~ msgstr "Rayon de noeud maximal"

# src/interface.c:107
#, fuzzy
#~ msgid "Ethernet"
#~ msgstr "Etherape"

# src/interface.c:270
#, fuzzy
#~ msgid "Solved"
#~ msgstr "Enregistrer"

# src/interface.c:278
#, fuzzy
#~ msgid "Add protocol"
#~ msgstr "Protocoles"

# src/main.c:76
#, fuzzy
#~ msgid "Remove protocol"
#~ msgstr "définir la couleur des noeuds"

#~ msgid "Capture"
#~ msgstr "Capture"

# src/main.c:79
#~ msgid "set the link color"
#~ msgstr "définir la couleur des liaisons"

#~ msgid "Toggle whether text is shown on the diagram"
#~ msgstr "Défini si le texte est affiché sur le diagramme"

#~ msgid "Click to toggle"
#~ msgstr "Cliquer pour sélectionner"

# src/capture.c:409
#, fuzzy
#~ msgid "Removing canvas_node. Number of node %d"
#~ msgstr "Suppression du noeud: %s. Nombre de noeuds: %d"

# src/capture.c:438 src/capture.c:443
#, fuzzy
#~ msgid "Removing canvas link. Number of links %d"
#~ msgstr "Suppression de la liaison. Nombre de liaisons: %d"

# src/capture.c:375
#~ msgid "Reached default in fill_names"
#~ msgstr "Comportement par défaut atteint dans fill_names"

# src/interface.c:23
#~ msgid "._New File"
#~ msgstr "_Nouveau fichier"

# src/interface.c:246
#~ msgid "New"
#~ msgstr "Nouveau"

# src/interface.c:23
#~ msgid "Open File"
#~ msgstr "Ouvrir fichier"

# src/interface.c:23
#~ msgid "Save File"
#~ msgstr "Enregistrer fichier"

# src/capture.c:388
#~ msgid "n/a"
#~ msgstr "n/a"

#~ msgid "2"
#~ msgstr "2"

#~ msgid "3"
#~ msgstr "3"

#~ msgid "4"
#~ msgstr "4"

#~ msgid "5"
#~ msgstr "5"

#~ msgid "6"
#~ msgstr "6"

#~ msgid "Filter what's captured. See the tcpdump man page for details"
#~ msgstr ""
#~ "Filtre ce qui est capturé. Voyez la page de man de tcpdump pour les "
#~ "détails"

# src/interface.c:247
#~ msgid "Nuevo archivo"
#~ msgstr "Nouvelle archive"

# src/interface.c:259
#~ msgid "Abrir archivo"
#~ msgstr "Ouvrir une archive"

# src/interface.c:271
#~ msgid "Guardar archivo"
#~ msgstr "Enregistrer l'archive"
